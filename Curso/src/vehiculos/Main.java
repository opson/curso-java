package vehiculos;

public class Main {
	
	public static void main(String args[]){
		
		//esto es un comentario
		
		Automovil jeep = new Automovil("JEEP", "Chiclamino azul", 3.7f, 8, 5, "Automatico");
		Automovil bocho = new Automovil();
		System.out.println("Jeep="+jeep.toString());
		System.out.println("Bocho="+bocho.toString());

		bocho.setMarca("VW");
		bocho.setPuertas(2);
		
		System.out.println("Bocho con marca="+bocho.toString());
		

		System.out.println("Bocho color="+bocho.getColor());
		System.out.println("Bocho numero de puertas="+bocho.getPuertas()+" puertas");

		
	}

}
