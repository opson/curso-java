package vehiculos;

public class Automovil {
	
	// Atributos
	private String marca;
	private String color;
	private float litros;
	private int cilindros;
	private int puertas;
	private String tipo;
	
	//Constructor
	public Automovil(){
		marca="Sin marca";
		color="blanco";
		litros=3.0f;
		cilindros=8;
		puertas=4;
		tipo="automatico";
	}
	
	public Automovil(String marca,String color,float litros,int cilindros,int puertas,String tipo){
	this.marca = marca;
	this.color = color;
	this.litros =litros;
	this.cilindros=cilindros;
	this.puertas = puertas;
	this.tipo = tipo;
	}

	
	//metodos
	
	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public float getLitros() {
		return litros;
	}

	public void setLitros(float litros) {
		this.litros = litros;
	}

	public int getCilindros() {
		return cilindros;
	}

	public void setCilindros(int cilindros) {
		this.cilindros = cilindros;
	}

	public int getPuertas() {
		return puertas;
	}

	public void setPuertas(int puertas) {
		this.puertas = puertas;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Automovil marca=" + marca + ", color=" + color + ", litros=" + litros + ", cilindros=" + cilindros
				+ ", puertas=" + puertas + ", tipo=" + tipo;
	}
	
}
